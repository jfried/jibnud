package Jibnu::Build::Configuration::XML;

use strict;
use warnings;

use XML::Simple;
use Data::Dumper;

use Jibnu::Build::Configuration;

use base qw(Jibnu::Build::Configuration);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(@_);

	bless($self, $proto);
	
	return $self;
}

sub get_file {
	my $self = shift;

	my $content = $self->SUPER::get_file(@_);
	if($main::opts{'debug'}) {
		print $content . "\n";
	}
	$self->{'__config'} = XMLin($content, ForceArray => 1);

	if($main::opts{'debug'}) {
		print Dumper($self->{'__config'});
	}
}

sub get_environments {
	my $self = shift;
	my @ret = ();

	foreach my $env (keys %{$self->{'__config'}->{'environments'}->[0]->{'environment'}}) {
		push(@ret, $env);
	}

	return @ret;
}

sub get_scm {
	my $self = shift;
	my $env = shift;

	return $self->{'__config'}->{'environments'}->[0]->{'environment'}->{$env}->{'scm'}->[0]->{'src'};
}

sub get_pkgs {
	my $self = shift;
	my $env = shift;

	my @ret = ();
	my $packages = $self->{'__config'}->{'environments'}->[0]->{'environment'}->{$env}->{'packages'}->[0]->{'package'};
	foreach my $pkg (keys %{$packages}) {
		push(@ret, $pkg);
	}

	return @ret;
}

sub get_pkg_vars {
	my $self = shift;
	my $env = shift;
	my $pkg = shift;

	my @ret = ();
	my $packages = $self->{'__config'}->{'environments'}->[0]->{'environment'}->{$env}->{'packages'}->[0]->{'package'}->{$pkg};
	foreach my $var (keys %{$packages->{'var'}}) {
		push(@ret, {
			name => $var,
			value => $packages->{'var'}->{$var}->{'value'}
		});
	}

	return @ret;
}

sub get_env_description {
	my $self = shift;
	my $env = shift;

	return $self->{'__config'}->{'environments'}->[0]->{'environment'}->{$env}->{'description'}->[0];
}

sub get_env_scm {
	my $self = shift;
	my $env = shift;

	return $self->{'__config'}->{'environments'}->[0]->{'environment'}->{$env}->{'scm'}->[0]->{'src'};
}


sub get_env_repo {
	my $self = shift;
	my $env = shift;

	return $self->{'__config'}->{'environments'}->[0]->{'environment'}->{$env}->{'repo'}->[0]->{'src'};
}

sub get_env_repo_rebuild_cmd {
	my $self = shift;
	my $env = shift;

	return $self->{'__config'}->{'environments'}->[0]->{'environment'}->{$env}->{'repo'}->[0]->{'rebuild'}->[0]->{'cmd'};
}



1;
