package Jibnu::Build::Configuration;

use strict;
use warnings;

use LWP::Simple qw(!get !head !post);
use Jibnu::Exception::FileNotFound;

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = { @_  };

	bless($self, $proto);

	$self->get_file($self->{'file'});
	
	return $self;
}

sub get_file {
	my $self = shift;
	my $file = shift;
	
	my $content = LWP::Simple::get($file);

	if(! $content || $content eq "") {
		Jibnu::Exception::FileNotFound->throw();
	}

	return $content;
}

1;
