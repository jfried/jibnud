package Jibnu::SCM::Subversion;

use strict;
use warnings;

use XML::Simple;
use Data::Dumper;

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = { @_ };

	bless($self, $proto);


	return $self;
}

sub chdir {
	my $self = shift;
	chdir($self->get_local_path());
}

sub get_uri {
	my $self = shift;
	return $self->{'uri'};
}

sub get_local_path {
	my $self = shift;
	return $self->{'local_path'};
}

sub get_mime_type {
	my $self = shift;
	my $entry = shift;

	my $path = $self->get_local_path() . '/' . $entry;
	my $mime_type = `/usr/bin/file -b --mime-type $path`;
	chomp $mime_type;

	return $mime_type;
}

sub get_file_content {
	my $self = shift;
	my $file = shift;

	my $path = $self->get_local_path() . '/' . $file;

	my $content;
	{ local $/ = undef; local *FILE; open FILE, "<$path"; $content = <FILE>; close FILE }

	return $content;
}

sub get_log {
	my $self = shift;
	my $path = shift;

	if(! $path) { $path = ""; }
	$self->chdir();
	my $log = `svn log --xml . 2>/dev/null`;
	my $l = XMLin($log);

	my $ret_log = {};

	foreach my $logentry (@{$l->{'logentry'}}) {
		my ($year, $mon, $day, $hour, $min) = ($logentry->{'date'} =~ m/^(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d).*$/);
		my $date = "$year-$mon-$day";

		if(! defined $ret_log->{$date}) {
			$ret_log->{$date} = [];
		}

		push(@{$ret_log->{$date}}, {
			msg => $logentry->{'msg'},
			revision => $logentry->{'revision'},
			date => "$year-$mon-$day $hour:$min",
			author => $logentry->{'author'}
		});
	}

	return $ret_log;
}

sub get_last_log {
	my $self = shift;
	my $file = shift;

	if(! $file) { $file = ""; }

	$self->chdir();
	my $uri = $self->get_uri() . '/' . $file;
	$uri =~ s/\/+$//g;
	my $last_log = `svn log --xml -l 1 $uri 2>/dev/null`;
	
	my $ll = XMLin($last_log);

	my ($year, $mon, $day, $hour, $min) = ($ll->{'logentry'}->{'date'} =~ m/^(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d).*$/);

	my $return = {
		author => $ll->{'logentry'}->{'author'},
		revision => $ll->{'logentry'}->{'revision'},
		date => "$year-$mon-$day $hour:$min UTC",
		msg => $ll->{'logentry'}->{'msg'}
	};

	return $return;
}

sub list_dir {
	my $self = shift;
	my $dir = shift;

	my @ret = ();
	my $path = $self->get_local_path() . '/' . $dir;
	local *DH;
	opendir(DH, $path);
	while(my $entry = readdir(DH)) {
		my $type = 'file';
		my $mime_type = "";
		next if ($entry =~ m/^\./);
		if(-d "$path/$entry") {
			$type = 'dir';
		}
		$mime_type = `/usr/bin/file -bi $path/$entry 2>/dev/null`;
		chomp $mime_type;

		push(@ret, {
			name => $entry,
			type => $type,
			mime_type => $mime_type
		});
	}
	closedir(DH);

	return @ret;
}

1;
