package Jibnu::Daemon::UNIX;

use Socket;
use IO::Handle;
use Jibnu::Daemon;

use base qw(Jibnu::Daemon);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(@_);
	
	bless($self, $proto);
	
	return $self;
}

sub start {
	my $self = shift;
	
	my $srv;
	socket($srv, PF_UNIX, SOCK_STREAM,0);
	unlink($self->{"unix-socket"});
	bind($srv,sockaddr_un($self->{"unix-socket"})) or die("ERROR! $!");
	listen($srv, 1);
	
	RUN_FOREVER:
	while(1) {
		SPAWN_THREADS:
		for(1..$self->{"workers"}) {
			threads->new(sub {
				$SIG{'HUP'} = sub {
					print "[*] Received SIGHUP, Quitting...\n";
					threads->self->exit;
				};
				my($server, $counter) = @_;
				$0 = "jibnud worker ($counter)";
				$main::threads_status{'thread-' . $counter} = "idle";
				
				$self->worker($server, $counter);
			}, $srv, $_);
		}

		# auf alle threads warten
		$_->join foreach threads->list;

		%main::threads_status = ();

		# config neu laden
		print "[*] Loading configuration\n";
		my $old_conf = $main::build_conf;
		eval {
			$main::build_conf = Jibnu::Build::Configuration::XML->new(file => $main::opts{'build-file'});
		};

		my $e;
		if($e = Exception::Class->caught('Jibnu::Exception::FileNotFound')) {
			print "[!] Could not retrieve configuration file. Check permissions.\n";
			print "[!] Using old configuration!\n";
			$main::build_conf = $old_conf;
		}
	}
}

1;
