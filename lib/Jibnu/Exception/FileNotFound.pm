package Jibnu::Exception::FileNotFound;

use strict;
use warnings;

use Exception::Class::Base;

use base qw(Exception::Class::Base);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(error => 'File Not Found', @_);

	bless($self, $proto);
	
	return $self;
}

1;
