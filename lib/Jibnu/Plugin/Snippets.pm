package Jibnu::Plugin::Snippets;

use strict;
use warnings;

use Jibnu::Plugin;

use Syntax::Highlight::Engine::Kate::All;
use Syntax::Highlight::Engine::Kate;

use base qw(Jibnu::Plugin);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(@_);

	bless($self, $proto);

	$self->{'__highlighter'} = {
		'bash' => 'Bash',
		'perl' => 'Perl',
		'makefile' => 'Makefile',
		'awk' => 'AWK',
		'c' => 'C',
		'c++' => 'C++',
		'changelog' => 'ChangeLog',
		'css' => 'CSS',
		'diff' => 'Diff',
		'java' => 'Java',
		'javascript' => 'JavaScript',
		'php' => 'PHP/PHP',
		'python' => 'Python',
		'ruby' => 'Ruby',
		'sql' => 'SQL',
		'xml' => 'XML'
	};

	return $self;
}

sub init {
	my $self = shift;

	$self->daemon->request->add_url('/snippets', sub {
		return $self->url_index();
	});

	$self->daemon->request->add_url('/snippets/browse', sub {
		return $self->url_browse();
	});

	$self->daemon->request->add_url_regexp('\/snippets\/view\/([a-zA-Z0-9\-]+)$', sub {
		return $self->url_view_snippet(@_);
	});

	$self->daemon->request->add_url_regexp('\/snippets\/comment\/([a-zA-Z0-9\-]+)\/add$', sub {
		return $self->url_comment_add(@_);
	});
}

sub _gen_snippet_id {
	my $self = shift;
	my @chars = qw(0 1 2 3 4 5 6 7 8 9
		a b c d e f g h i j k l m n o p q r s t u v w x y z);
	my $ret = "";

	for(1..4) {
		for(1..4) {
			$ret .= $chars[int(rand(scalar(@chars)))];
		}
		$ret .= "-";
	}
	$ret =~ s/-$//;

	return $ret;
}

sub url_comment_add {
	my $self = shift;
	my $snippet_id = shift;

	if($self->daemon->request->get_request_method() eq "POST") {
		my $r = $self->daemon->request;
		if($r->param('author') && $r->param('title') && $r->param('message')) {
			my $sth = $self->daemon->db->prepare("INSERT INTO `snippets_comments` (snippet_id, email, title, message) VALUES(?, ?, ?, ?)");
			$sth->bind_param(1, $snippet_id);
			$sth->bind_param(2, $r->param('author'));
			$sth->bind_param(3, $r->param('title'));
			$sth->bind_param(4, $r->param('message'));
			$sth->execute();
		}
	}

	return Jibnu::Protocol::Server::HTTP::Request::Redirect->new('/snippets/view/' . $snippet_id);
}

sub url_view_snippet {
	my $self = shift;
	my $snippet_id = shift;

	my $sth = $self->daemon->db->prepare("SELECT * FROM snippets WHERE snippet_id=? ORDER BY created DESC LIMIT 1");
	$sth->bind_param(1, $snippet_id);
	$sth->execute();

	my $snippet = {};
	my $row = $sth->fetchrow_hashref();
	if($row) {
		my $content = $row->{'content'};
		$content =~ s/\r//gms;
		my @t = split(/\n/, $content);
		my @file_content = ();
		foreach my $line (@t) {
			if($self->{'__highlighter'}->{$row->{'source'}}) {
				push(@file_content, $self->hl(source => $row->{'source'})->highlightText($line));
			} else {
				push(@file_content, $self->escape_html($content, 1));
			}
		}

		$snippet = {
			source => $self->escape_html($row->{'source'}),
			name => $self->escape_html($row->{'name'}),
			email => $self->escape_html($row->{'email'}),
			content => \@file_content,
			snippet_id => $row->{'snippet_id'}
		};
	}
	$sth->finish();

	$sth = $self->daemon->db->prepare("SELECT * FROM snippets_comments WHERE snippet_id=? ORDER BY created DESC");
	$sth->bind_param(1, $snippet_id);
	$sth->execute();

	my @comments = ();
	while(my $c_row = $sth->fetchrow_hashref()) {
		my ($created) = ($c_row->{'created'} =~ m/^(\d\d\d\d-\d\d-\d\d \d\d:\d\d).*$/);
		push(@comments, {
			snippet_id => $c_row->{'snippet_id'},
			email => $self->escape_html($c_row->{'email'}),
			title => $self->escape_html($c_row->{'title'}),
			message => $self->escape_html($c_row->{'message'}),
			created => $created
		});
	}

	my $snippet_url = 'http://' . $self->daemon->request->get_header('Host') . '/snippets/view/' . $snippet->{'snippet_id'};

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon,
								template_vars => [
									sub_area => 'browse',
									snippet => $snippet,
									comments => \@comments,
									snippet_url => $snippet_url
								],
								template => 'snippet_view.html');
}

sub url_index {
	my $self = shift;


	if($self->daemon->request->get_request_method() eq "POST") {
		my $r = $self->daemon->request;
		if($r->param('author') && $r->param('source') && $r->param('txtcode') && $r->param('name')) {
			my $snippet_id = $self->_gen_snippet_id();
			my $sth = $self->daemon->db->prepare("INSERT INTO `snippets` (source, name, email, content, snippet_id) VALUES(?, ?, ?, ?, ?)");
			$sth->bind_param(1, $r->param('source'));
			$sth->bind_param(2, $r->param('name'));
			$sth->bind_param(3, $r->param('author'));
			$sth->bind_param(4, $r->param('txtcode'));
			$sth->bind_param(5, $snippet_id);
			$sth->execute();

			return Jibnu::Protocol::Server::HTTP::Request::Redirect->new('/snippets/view/' . $snippet_id);
		}

	}


	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon,
								template_vars => [
									sub_area => 'index'
								],
								template => 'snippets.html');
}

sub hl {
	my $self = shift;
	my $p = { @_ };

	my $hl;

	if(defined $self->{'__highlighter'}->{$p->{"source"}}) {
		$hl = Syntax::Highlight::Engine::Kate->new(
			language => $self->{'__highlighter'}->{$p->{"source"}},
			substitutions => {
				"<" => "&lt;",
				">" => "&gt;",
				"&" => "&amp;",
				" " => "&nbsp;",
				"\t" => "&nbsp;&nbsp;&nbsp;",
				"\n" => "<br />\n",
			},
			format_table => {
				Alert => ["<font color=\"#0000ff\">", "</font>"],
				BaseN => ["<font color=\"#007f00\">", "</font>"],
				BString => ["<font color=\"#c9a7ff\">", "</font>"],
				Char => ["<font color=\"#ff00ff\">", "</font>"],
				Comment => ["<font color=\"#7f7f7f\"><i>", "</i></font>"],
				DataType => ["<font color=\"#0000ff\">", "</font>"],
				DecVal => ["<font color=\"#00007f\">", "</font>"],
				Error => ["<font color=\"#ff0000\"><b><i>", "</i></b></font>"],
				Float => ["<font color=\"#00007f\">", "</font>"],
				Function => ["<font color=\"#007f00\">", "</font>"],
				IString => ["<font color=\"#ff0000\">", ""],
				Keyword => ["<b>", "</b>"],
				Normal => ["", ""],
				Operator => ["<font color=\"#ffa500\">", "</font>"],
				Others => ["<font color=\"#b03060\">", "</font>"],
				RegionMarker => ["<font color=\"#96b9ff\"><i>", "</i></font>"],
				Reserved => ["<font color=\"#9b30ff\"><b>", "</b></font>"],
				String => ["<font color=\"#ff0000\">", "</font>"],
				Variable => ["<font color=\"#0000ff\"><b>", "</b></font>"],
				Warning => ["<font color=\"#0000ff\"><b><i>", "</b></i></font>"],
			}
		);
	}

	return $hl;
}

sub url_browse {
	my $self = shift;

	my $sth = $self->daemon->db->prepare("SELECT * FROM snippets GROUP BY snippet_id ORDER BY created DESC LIMIT 10");
	$sth->execute();

	my @snippets = ();
	while(my $row = $sth->fetchrow_hashref()) {
		my $content = $row->{'content'};
		$content =~ s/\r//gms;
		my @t = split(/\n/, $content);
		my @file_content = ();
		foreach my $line (@t) {
			if($self->{'__highlighter'}->{$row->{'source'}}) {
				push(@file_content, $self->hl(source => $row->{'source'})->highlightText($line));
			} else {
				push(@file_content, $self->escape_html($content, 1));
			}
		}

		push(@snippets, {
			source => $self->escape_html($row->{'source'}),
			name => $self->escape_html($row->{'name'}),
			email => $self->escape_html($row->{'email'}),
			content => \@file_content,
			snippet_id => $row->{'snippet_id'}
		});
	}

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon,
								template_vars => [
									sub_area => 'browse',
									snippets => \@snippets
								],
								template => 'snippets_browse.html');

}

1;
