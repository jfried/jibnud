package Jibnu::Plugin::Status;

use strict;
use warnings;

use Jibnu::Plugin;

use base qw(Jibnu::Plugin);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(@_);

	bless($self, $proto);

	return $self;
}

sub init {
	my $self = shift;

	$self->daemon->request->add_url('/status', sub {
		return $self->url_status();
	});
}

sub url_status {
	my $self = shift;

	my @ret = ();

	foreach my $thr (keys %main::threads_status) {
		push(@ret, {
			name => $thr,
			status => $main::threads_status{$thr}
		});
	}

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
									template => 'status.html', 
									template_vars => [ threads => \@ret ]);
}

1;
