package Jibnu::Plugin::Environments;

use strict;
use warnings;

use Jibnu::Plugin;

use base qw(Jibnu::Plugin);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(@_);

	bless($self, $proto);

	return $self;
}

sub init {
	my $self = shift;

	foreach my $env ($main::build_conf->get_environments()) {
		$self->daemon->request->add_url('/environments/' . $env, sub {
			return $self->url_environment($env);
		});
	}

	$self->daemon->request->add_url('/environments', sub {
		return $self->url_environments();
	});
}

sub url_environments {
	my $self = shift;

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
								template => 'environments.html');
}

sub url_environment {
	my $self = shift;
	my $env = shift;

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
								template => 'environment.html',
								template_vars => [env => $env]);
}

1;
