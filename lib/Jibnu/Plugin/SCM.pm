package Jibnu::Plugin::SCM;

use strict;
use warnings;

use Syntax::Highlight::Engine::Kate::All;
use Syntax::Highlight::Engine::Kate;

use Jibnu::Plugin;
use Jibnu::SCM::Subversion;

use base qw(Jibnu::Plugin);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(@_);

	bless($self, $proto);

	$self->{'__highlighter'} = {
		'text/x-shellscript' => 'Bash'
	};

	return $self;
}

sub init {
	my $self = shift;

	$self->daemon->request->add_url('/scm', sub {
		return $self->url_index();
	});

	$self->daemon->request->add_url('/scm/update', sub {
		return $self->url_scm_update();
	});
	
	$self->daemon->request->add_url_regexp('\/scm\/browse\/([a-zA-Z0-9_\-\.]+)$', sub {
		return $self->url_browse_scm(@_);
	});

	$self->daemon->request->add_url_regexp('\/scm\/commits\/([a-zA-Z0-9_\-\.]+)$', sub {
		return $self->url_commits_scm(@_);
	});

	$self->daemon->request->add_url_regexp('\/scm\/comments\/([a-zA-Z0-9_\-\.]+)$', sub {
		return $self->url_comments_scm(@_);
	});

	$self->daemon->request->add_url_regexp('\/scm\/comments\/([a-zA-Z0-9_\-\.]+)/add$', sub {
		return $self->url_comment_add_scm(@_);
	});

	$self->daemon->request->add_url_regexp('\/scm\/browse\/([a-zA-Z0-9_\-\.]+)\/(.*)$', sub {
		return $self->url_browse_scm(@_);
	});

	$self->daemon->request->add_url_regexp('\/scm\/view\/([a-zA-Z0-9_\-\.]+)\/(.*)$', sub {
		return $self->url_view_scm(@_);
	});

	$self->daemon->request->add_url_regexp('\/scm\/log\/([a-zA-Z0-9_\-\.]+)/(.*)$', sub {
		return $self->url_log_scm(@_);
	});



}

sub scm {
	my $self = shift;
	my $env = shift;
	
	return Jibnu::SCM::Subversion->new(uri => $main::build_conf->get_env_scm($env), 
						local_path => $main::scm_checkout_dir . $env);
}

sub _get_last_log {
	my $self = shift;
	my $env = shift;
	my $path = shift;

	my $last_log = $self->daemon->cache->get("$env/$path/scm-last-log");
	if(! defined $last_log) {
		$last_log = $self->scm($env)->get_last_log($path);
		$self->daemon->cache->set("$env/$path/scm-last-log", $last_log, "10 minutes");
	}

	return $last_log;
}

sub url_scm_update {
	my $self = shift;

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
									template => 'scm_update.html', 
									shutdown_action => sub {
										$self->shutdown_action_update_scm();
									});
}

sub shutdown_action_update_scm {
	my $self = shift;

	print "[*] Updating / Checking out all SCMs\n";
	foreach my $env ($main::build_conf->get_environments()) {
		$self->daemon->request->set_status('updating scm: ' . $env);
		print "   > $env\n";
		if( ! -d "$main::scm_checkout_dir$env") {
			# kompletter checkout
			mkdir("$main::scm_checkout_dir$env", 0777);
			chdir("$main::scm_checkout_dir$env");
			system("svn co " . $main::build_conf->get_env_scm($env) . " . >/dev/null 2>&1");
			if($? != 0) {
				print "[!!] WARN: Error checking out $env\n";
			}
		} else {
			# nur updaten
			chdir("$main::scm_checkout_dir$env");
			system("svn up >/dev/null 2>&1");
			if($? != 0) {
				print "[!!] WARN: Error on SCM update ($env)\n";
			}
		}
	}
	$self->daemon->request->set_status('idle');
}

sub url_log_scm {
	my $self = shift;
	my $env = shift;
	my $path = shift;

	my $last_log = $self->_get_last_log($env, $path);

	my @current_path_array = split(/\//, $path);

	if($path !~ m/\/$/) { $path .= '/'; }
	if($path !~ m/^\//) { $path = '/' . $path; }

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
								template_vars => [
									env => $env,
									last_log => $last_log,
									sub_area => 'log',
									current_path => $path,
									current_path_array => \@current_path_array
								],
								template => 'scm_log.html');
}

sub url_comment_add_scm {
	my $self = shift;
	my $env = shift;
	my $path = "";

	my $last_log = $self->_get_last_log($env, $path);

	my @current_path_array = split(/\//, $path);

	if($path !~ m/\/$/) { $path .= '/'; }
	if($path !~ m/^\//) { $path = '/' . $path; }

	if($self->daemon->request->get_request_method() eq "POST") {
		my $r = $self->daemon->request;
		if($r->param('author') && $r->param('title') && $r->param('message')) {
			my $sth = $self->daemon->db->prepare("INSERT INTO `scm_comments` (env, email, title, message) VALUES(?, ?, ?, ?)");
			$sth->bind_param(1, $env);
			$sth->bind_param(2, $r->param('author'));
			$sth->bind_param(3, $r->param('title'));
			$sth->bind_param(4, $r->param('message'));
			$sth->execute();
		}

		return Jibnu::Protocol::Server::HTTP::Request::Redirect->new('/scm/comments/' . $env);
	}

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
								template_vars => [
									env => $env,
									last_log => $last_log,
									sub_area => 'comments',
									current_path => $path,
									current_path_array => \@current_path_array
								],
								template => 'scm_comment_add.html');
}

sub url_comments_scm {
	my $self = shift;
	my $env = shift;
	my $path = "";

	my $last_log = $self->_get_last_log($env, $path);

	my @current_path_array = split(/\//, $path);

	if($path !~ m/\/$/) { $path .= '/'; }
	if($path !~ m/^\//) { $path = '/' . $path; }

	my $sth = $self->daemon->db->prepare("SELECT * FROM scm_comments WHERE `env`=?");
	$sth->bind_param(1, $env);
	$sth->execute();
	my @comments = ();
	while(my $row = $sth->fetchrow_hashref()) {
		push(@comments, {
			message => $self->escape_html($row->{'message'}),
			email => $self->escape_html($row->{'email'}),
			title => $self->escape_html($row->{'title'})
		});
	}

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
								template_vars => [
									env => $env,
									last_log => $last_log,
									sub_area => 'comments',
									comments => \@comments,
									current_path => $path,
									current_path_array => \@current_path_array
								],
								template => 'scm_comments.html');
}

sub url_commits_scm {
	my $self = shift;
	my $env = shift;
	my $path = "";

	my $last_log = $self->_get_last_log($env, $path);

	my @current_path_array = split(/\//, $path);

	my $commits = $self->daemon->cache->get("$env/commits");
	if(! defined $commits) {
		$commits = $self->scm($env)->get_log($path);
		$self->daemon->cache->set("$env/commits", $commits, "10 minutes");
	}

	if($path !~ m/\/$/) { $path .= '/'; }
	if($path !~ m/^\//) { $path = '/' . $path; }

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
								template_vars => [
									env => $env,
									last_log => $last_log,
									sub_area => 'commits',
									commits => $commits,
									current_path => $path,
									current_path_array => \@current_path_array
								],
								template => 'scm_commits.html');

}

sub url_view_scm {
	my $self = shift;
	my $env = shift;
	my $path = shift;

	my @current_path_array = split(/\//, $path);

	my $last_log = $self->_get_last_log($env, $path);

	my $mime_type = $self->scm($env)->get_mime_type($path);
	my $file_content = "";
	if(defined $self->{'__highlighter'}->{$mime_type}) {
		my $hl = Syntax::Highlight::Engine::Kate->new(
			language => $self->{'__highlighter'}->{$mime_type},
			substitutions => {
				"<" => "&lt;",
				">" => "&gt;",
				"&" => "&amp;",
				" " => "&nbsp;",
				"\t" => "&nbsp;&nbsp;&nbsp;",
				"\n" => "<br />\n",
			},
			format_table => {
				Alert => ["<font color=\"#0000ff\">", "</font>"],
				BaseN => ["<font color=\"#007f00\">", "</font>"],
				BString => ["<font color=\"#c9a7ff\">", "</font>"],
				Char => ["<font color=\"#ff00ff\">", "</font>"],
				Comment => ["<font color=\"#7f7f7f\"><i>", "</i></font>"],
				DataType => ["<font color=\"#0000ff\">", "</font>"],
				DecVal => ["<font color=\"#00007f\">", "</font>"],
				Error => ["<font color=\"#ff0000\"><b><i>", "</i></b></font>"],
				Float => ["<font color=\"#00007f\">", "</font>"],
				Function => ["<font color=\"#007f00\">", "</font>"],
				IString => ["<font color=\"#ff0000\">", ""],
				Keyword => ["<b>", "</b>"],
				Normal => ["", ""],
				Operator => ["<font color=\"#ffa500\">", "</font>"],
				Others => ["<font color=\"#b03060\">", "</font>"],
				RegionMarker => ["<font color=\"#96b9ff\"><i>", "</i></font>"],
				Reserved => ["<font color=\"#9b30ff\"><b>", "</b></font>"],
				String => ["<font color=\"#ff0000\">", "</font>"],
				Variable => ["<font color=\"#0000ff\"><b>", "</b></font>"],
				Warning => ["<font color=\"#0000ff\"><b><i>", "</b></i></font>"],
			}
		);
		$file_content = $hl->highlightText($self->scm($env)->get_file_content($path));
	} else {
		$file_content = $self->scm($env)->get_file_content($path);
		$file_content =~ s/ /&nbsp;/gms;
		$file_content =~ s/\t/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/gms;
	}

	$file_content =~ s/\r//gms;
	my @file_content = split(/\n/, $file_content);

	if($path !~ m/\/$/) { $path .= '/'; }
	if($path !~ m/^\//) { $path = '/' . $path; }


	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
								template_vars => [
									env => $env,
									last_log => $last_log,
									current_path => $path,
									sub_area => 'browse',
									current_path_array => \@current_path_array,
									file_content => \@file_content
								],
								template => 'scm_view.html');
}

sub url_browse_scm {
	my $self = shift;
	my $env = shift;
	my $path = shift;

	if(! $path) { $path = ""; }

	my $last_log = $self->_get_last_log($env, $path);

	my $listing = $self->daemon->cache->get("$env/scm-listing-$path");
	if(! defined $listing) {
		my @t = $self->scm($env)->list_dir($path);
		$listing = \@t;
		$self->daemon->cache->set("$env/scm-listing-path", $listing, "2 minutes");
	}

	my @current_path_array = split(/\//, $path);

	if($path !~ m/\/$/) { $path .= '/'; }
	if($path !~ m/^\//) { $path = '/' . $path; }

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
								template_vars => [
									env => $env,
									last_log => $last_log,
									listing => $listing,
									sub_area => 'browse',
									current_path => $path,
									current_path_array => \@current_path_array
								],
								template => 'scm_browse.html');
}

sub url_index {
	my $self = shift;

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, template => 'scm.html');
}

1;
