package Jibnu::Plugin::Index;

use strict;
use warnings;

use Jibnu::Plugin;

use base qw(Jibnu::Plugin);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(@_);

	bless($self, $proto);

	return $self;
}

sub init {
	my $self = shift;

	$self->daemon->request->add_url('/', sub {
		return $self->url_index();
	});

	$self->daemon->request->add_url('/reload', sub {
		return $self->url_reload();
	});

	$self->daemon->request->add_url('', sub {
		return $self->url_index();
	});
}

sub url_index {
	my $self = shift;

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, template => 'index.html');
}

sub url_reload {
	my $self = shift;

	print "[*] Updating / Checking out all SCMs\n";
	foreach my $env ($main::build_conf->get_environments()) {
		$self->daemon->request->set_status('updating scm: ' . $env);
		print "   > $env\n";
		if( ! -d "$main::scm_checkout_dir$env") {
			# kompletter checkout
			mkdir("$main::scm_checkout_dir$env", 0777);
			chdir("$main::scm_checkout_dir$env");
			system("svn co " . $main::build_conf->get_env_scm($env) . " . >/dev/null 2>&1");
			if($? != 0) {
				print "[!!] WARN: Error checking out $env\n";
			}
		} else {
			# nur updaten
			chdir("$main::scm_checkout_dir$env");
			system("svn up >/dev/null 2>&1");
			if($? != 0) {
				print "[!!] WARN: Error on SCM update ($env)\n";
			}
		}
	}

	my @running = threads->list(threads::running);
	foreach my $thr (@running) {
		print "[*] Quitting thread: " . $thr->tid . "\n";
		$thr->kill('SIGHUP');
	}

	return Jibnu::Protocol::Server::HTTP::Request::Redirect->new('/', shutdown_action => sub {
		print "[*] Quitting myself\n";
		threads->self->exit;
	});
}

1;
