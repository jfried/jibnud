package Jibnu::Plugin::Build;

use strict;
use warnings;

use Jibnu::Plugin;
use Jibnu::FileTransfer::SFTP;
use Jibnu::RemoteCommand::SSH;

use base qw(Jibnu::Plugin);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(@_);

	bless($self, $proto);

	return $self;
}

sub init {
	my $self = shift;

	foreach my $env ($main::build_conf->get_environments()) {
		$self->daemon->request->add_url('/build/' . $env, sub {
			return $self->url_build_env($env);
		});
	}

	$self->daemon->request->add_url_regexp('^\/build\/(.*?)\/package\/(.*)$', sub {
		return $self->url_build_package(@_);
	});
}

sub url_build_package {
	my $self = shift;
	my $env = shift;
	my $pkg = shift;

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
									template => 'build_pkg.html', 
									template_vars => [
										env => $env,
										pkg => $pkg
									],
									shutdown_action => sub {
										$self->shutdown_action_build_pkg($env, $pkg);
									});
}

sub url_build {
	my $self = shift;
	
	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
									template => 'build_env.html', 
									template_vars => [],
									shutdown_action => sub {
										$self->shutdown_action();
									});
}

sub url_build_env {
	my $self = shift;
	my $env = shift;

	my @ret = ();

	return Jibnu::Protocol::Server::HTTP::Request::OK->new(daemon => $self->daemon, 
									template => 'build_env.html', 
									template_vars => [ env => $env ],
									shutdown_action => sub {
										$self->shutdown_action();
									});
}

sub shutdown_action_build_pkg {
	my $self = shift;
	my $env = shift;
	my $pkg = shift;

	sleep 5;

	$self->{'build'} = $env;
	$self->daemon->request->set_status('building ' . $pkg . '/' . $env);

	$self->start_build($pkg);

	$self->{'build'} = '';
	$self->daemon->request->set_status('idle');
}

sub shutdown_action {
	my $self = shift;

	sleep 5;

	my ($build) = ($self->daemon->request->get_uri() =~ m/^\/build\/(.*)$/);
	$self->{'build'} = $build;
	$self->daemon->request->set_status('building ' . $build);

	$self->start_build();

	$self->{'build'} = '';
	$self->daemon->request->set_status('idle');
}

sub _set_status {
	my $self = shift;
	my $status = shift;
	my $id = shift;
	my $pkg = shift;
	my $cmdout = shift;

	if(!$id) {
		my $stm = $self->daemon->db->prepare("INSERT INTO buildstatus (env, pkg, status) VALUES(?, ?, ?)");
		$stm->bind_param(1, $self->{'build'});
		$stm->bind_param(2, $pkg);
		$stm->bind_param(3, $status);
		$stm->execute();
		my $i_id = $self->daemon->db->last_insert_id(undef, "jibnu", "buildstatus", "id");
		return $i_id;
	} else {
		my $stm = $self->daemon->db->prepare("UPDATE buildstatus SET `status`=?, `cmdoutput`=? WHERE id=$id");
		$stm->bind_param(1, $status);
		$stm->bind_param(2, $cmdout);
		$stm->execute();
	}
}

sub start_build {
	my $self = shift;
	my $pkg = shift;

	my @pkgs = $main::build_conf->get_pkgs($self->{'build'});
	if($pkg) {
		@pkgs = ($pkg);
	}

	BUILD_PKG:
	foreach my $pkg (@pkgs) {
		my $cmdout = "";
		my $i_id = $self->_set_status('downloading', undef, $pkg);
		$cmdout = $self->export_pkg($pkg);

		$self->_set_status('postinst', $i_id, $pkg, $cmdout);
		$cmdout .= $self->create_postinst($pkg);

		$self->_set_status('build', $i_id, $pkg, $cmdout);
		my @ret = $self->build_pkg($pkg);
		$cmdout .= $ret[1];

		if($ret[0] != 0) {
			$self->_set_status('failed', $i_id, $pkg, $cmdout);
		}

		$self->_set_status('uploading', $i_id, $pkg, $cmdout);
		@ret = $self->upload_pkg($pkg);

		if($ret[0] != 0) {
			$self->_set_status('failed', $i_id, $pkg, $cmdout);
			next BUILD_PKG;
		}

		$self->_set_status('recreating repo', $i_id, $pkg, $cmdout);
		@ret = $self->recreate_repo();

		if($ret[0] != 0) {
			$self->_set_status('failed', $i_id, $pkg, $cmdout);
			next BUILD_PKG;
		}
		$self->_set_status('done', $i_id, $pkg, $cmdout);
	}
}

sub create_postinst {
	my $self = shift;
	my $pkg = shift;

	print "[*] creating postinst of " . $self->{'build'} . "\n";

	my @postinst;
	{ local *FILE; open FILE, "<debian/postinst"; @postinst = <FILE>; close FILE }
	shift @postinst; # shebang wegmachen
	chomp @postinst;

	local *FH;
	open(FH, ">debian/postinst");
	print FH "#!/bin/bash\n\n"; # shebang schreiben
	# variablen schreiben
	foreach my $var ($main::build_conf->get_pkg_vars($self->{'build'}, $pkg)) {
		print FH "export " . $var->{"name"} . "=" . '"' . $self->quote($var->{"value"}) . '"' . "\n";
	}
	print FH "export JIBNU=1\n";
	print FH "export ENVIRON='" . $self->{"build"} . "'\n";
	
	# normalen content schreiben
	print FH join("\n", @postinst);
	close(FH);

	return "";
}

sub recreate_repo {
	my $self = shift;

	my ($proto, $user, $server, $repo_path) = ($main::build_conf->get_env_repo($self->{'build'}) =~ m/^([a-z]+):\/\/([a-zA-Z0-9_\-]+)\@([a-zA-Z0-9\.\-_]+)\/(.*)$/);
	$self->rcommand(server => $server, user => $user)->execute($main::build_conf->get_env_repo_rebuild_cmd($self->{'build'}));

	return (0, 'recreate repo');
}

sub upload_pkg {
	my $self = shift;
	my $pkg = shift;

	chdir($main::build_dir . $$);
	my $file = `ls -1 $pkg*.deb`;
	chomp $file;

	my ($proto, $user, $server, $repo_path) = ($main::build_conf->get_env_repo($self->{'build'}) =~ m/^([a-z]+):\/\/([a-zA-Z0-9_\-]+)\@([a-zA-Z0-9\.\-_]+)\/(.*)$/);

	$self->transfer(proto => $proto, server => $server, user => $user)->put($main::build_dir . $$ . '/' . $file, $repo_path);

	return (0, 'done');
}

sub rcommand {
	my $self = shift;
	return Jibnu::RemoteCommand::SSH->new(@_);
}

sub transfer {
	my $self = shift;

	return Jibnu::FileTransfer::SFTP->new(@_);
}

sub quote {
	my $self = shift;
	my $s = shift;

	$s =~ s/"/\\"/gms;

	return $s;
}

sub build_pkg {
	my $self = shift;
	my $pkg = shift;

	my @out = `dpkg-buildpackage -uc -us -rfakeroot 2>&1`;
	chomp @out;
	return ($?, join("\n", @out));
}

sub export_pkg {
	my $self = shift;
	my $pkg = shift;

	my $scm_path = $main::build_conf->get_scm($self->{'build'}) . $pkg;
	print "[*] exporting from: " . $scm_path . "\n";

	system("rm -rf $main::build_dir" . $$ . '/' . $pkg . " >/dev/null 2>&1");

	mkdir($main::build_dir . $$);
	chdir($main::build_dir . $$);
	my @out = `svn export $scm_path`;
	chomp @out;
	chdir($main::build_dir . $$ . '/' . $pkg);

	return join("\n", @out);
}



1;
