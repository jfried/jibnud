package Jibnu::Protocol::Server::HTTP;

use strict;
use warnings;

use Data::Dumper;
use Jibnu::Protocol::Server;

# Exceptions
use Jibnu::Exception::Server::HTTP::BadRequest;

# Responses
use Jibnu::Protocol::Server::HTTP::Request::OK;
use Jibnu::Protocol::Server::HTTP::Request::Bad;
use Jibnu::Protocol::Server::HTTP::Request::Redirect;

use base qw(Jibnu::Protocol::Server);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(@_);

	bless($self, $proto);

	$self->{'__headers'} = {};
	$self->{'__requested_uri'} = '';
	$self->{'__urls'} = {};
	$self->{'__urls_rx'} = {};
	$self->{'__request_params'} = {};

	return $self;
}

sub process {
	my $self = shift;

	$self->set_status('processing request');

	my $buf = $self->request;
	READ_HEADER:
		while(my $line = <$buf>) {
			chomp $line;
			$line =~ s/\r//gms;
			if($line =~ m/^$/) {
				last READ_HEADER;
			}

			my $e;
			eval { $self->_parse_header_line($line); };

			if($e = Exception::Class->caught('Jibnu::Exception::Server::HTTP::BadRequest')) {
				return Jibnu::Protocol::Server::HTTP::Request::Bad->new($self->{'daemon'});
			}
		}

	if($self->{'__method'} eq "POST") {
		my $data;
		$buf->read($data, $self->{'__headers'}->{'Content-Length'});
		$self->{'__content'} = $data;
		$self->_parse_content($data);
	}

	if(exists($self->{'__urls'}->{$self->get_uri()})) {
		my $cal = $self->{'__urls'}->{$self->get_uri()};
		return &$cal();
	} elsif($self->check_uri_rx($self->get_uri())) {
		my ($cal, $opts) = $self->check_uri_rx($self->get_uri());
		return &$cal( @{$opts} );
	} else {
		return Jibnu::Protocol::Server::HTTP::Request::Bad->new($self->{'daemon'});
	}
}

sub _parse_content {
	my $self = shift;
	my $content = shift;

	# author=sdf&title=sdf&message=sdf
	foreach my $tupel (split(/[&;]/, $content)) {
		my($key, $val) = split(/=/, $tupel);
		$val =~ s/\+/ /g;
		$key =~ s/\+/ /g;

		$val =~ s/%(..)/pack("C",hex($1))/ge;
		$key =~ s/%(..)/pack("C",hex($1))/ge;


		$self->{'__request_params'}->{$key} = $val;
	}
}

sub param {
	my $self = shift;
	my $key = shift;

	if(! defined $self->{'__request_params'}->{$key}) { return undef; }
	return $self->{'__request_params'}->{$key};
}

sub get_request_method {
	my $self = shift;
	return $self->{'__method'};
}

sub get_header {
	my $self = shift;
	my $key = shift;
	
	if(! defined $self->{'__headers'}->{$key}) { return undef; }
	return $self->{'__headers'}->{$key};
}

sub content {
	my $self = shift;
	return $self->{'__content'};
}

sub add_url($&) {
	my $self = shift;
	my $url = shift;
	my $f = shift;

	$self->{'__urls'}->{$url} = $f;
}

sub add_url_regexp($&) {
	my $self = shift;
	my $url = shift;
	my $f = shift;

	$self->{'__urls_rx'}->{$url} = $f;
}

sub check_uri_rx {
	my $self = shift;
	my $uri = shift;

	foreach my $rx (keys %{$self->{'__urls_rx'}}) {
		if($uri =~ m/$rx/) {
			my @opts = ($uri =~ m/$rx/);
			return ($self->{'__urls_rx'}->{$rx}, \@opts);
		}
	}

	return 0;
}

sub get_uri {
	my $self = shift;
	return $self->{'__requested_uri'};
}

sub _parse_header_line {
	my $self = shift;
	my $line = shift;

	if($line =~ m/^(GET|POST) .*$/ || $line =~ m/^([a-zA-Z0-9-_.]+):\s?(.*)$/) {
		if($line =~ m/^(GET|POST) (.*?) HTTP\/(.*?)/) {
			$self->{'__requested_uri'} = $2;
			$self->{'__method'} = $1;
		} else {
			$self->{'__headers'}->{$1} = $2;
		}
	} else {
		# bad request
		Jibnu::Exception::Server::HTTP::BadRequest->throw();	
	}
}

1;
