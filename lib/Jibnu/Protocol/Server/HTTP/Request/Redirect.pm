package Jibnu::Protocol::Server::HTTP::Request::Redirect;

use strict;
use warnings;

use HTTP::Response;

use base qw(HTTP::Response);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $redir = shift;
	my $self = $proto->SUPER::new(301, "Redirect", [ 'Location' => $redir ]);

	bless($self, $proto);

	my $opts = { @_ };
	foreach my $opt (keys %{$opts}) {
		$self->{$opt} = $opts->{$opt};
	}
	
	return $self;
}

sub shutdown_action {
	my $self = shift;
	my $cal = $self->{'shutdown_action'};
	if($cal) { &$cal(); }
}


1;
