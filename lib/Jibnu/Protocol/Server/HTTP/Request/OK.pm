package Jibnu::Protocol::Server::HTTP::Request::OK;

use strict;
use warnings;

use HTTP::Response;
use Text::Template::Simple;
use JSON::XS;

use base qw(HTTP::Response);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(200, "OK", [ 'Content-Type' => 'text/html; charset=UTF-8', 'Server' => 'JibnuD', 'Status' => 200 ]);

	my $opts = { @_ };
	foreach my $opt (keys %{$opts}) {
		$self->{$opt} = $opts->{$opt};
	}

	bless($self, $proto);

	$self->load_request();
	
	return $self;
}

sub json {
	my $self = shift;
	return JSON::XS->new();
}

sub tt {
	my $self = shift;
	my $tt = Text::Template::Simple->new();
	if($main::opts{'debug'}) {
		$tt->DEBUG(1);
	}

	return $tt;
}

sub load_request {
	my $self = shift;

	my $accept = $self->{'daemon'}->request->get_header('Accept');
	my $content = "";

	if($accept =~  m/application\/json/i) {
		my %tv = @{ $self->{'template_vars'} };
		$content = $self->json->encode(\%tv);
		$self->header("Content-Type" => 'application/json; charset=UTF-8');
	} else {
		chdir($main::template_dir);
		my ($area) = ($self->{'daemon'}->request->get_uri() =~ m/^\/([a-zA-Z0-9]+)/);
		$area = 'index' unless($area);

		my $mod_content = $self->tt->compile($main::template_dir . '/' . $self->{'template'}, $self->{"template_vars"});

		# last errors
		my $sth = $self->{'daemon'}->db->prepare("SELECT id, env, pkg, modified FROM buildstatus WHERE `status`='failed' ORDER BY modified DESC LIMIT 5");
		$sth->execute();
		my @rows = ();
		while(my $row = $sth->fetchrow_hashref()) {
			push(@rows, $row);
		}
		$sth->finish();

		# last builds
		$sth = $self->{'daemon'}->db->prepare("SELECT id, env, pkg, modified FROM buildstatus WHERE `status`='done' ORDER BY modified DESC LIMIT 5");
		$sth->execute();
		my @rows_ok = ();
		while(my $row = $sth->fetchrow_hashref()) {
			push(@rows_ok, $row);
		}

		$content = $self->tt->compile($main::template_dir . '/layout.html', [content => $mod_content, 
												build_errors => \@rows,
												build_ok => \@rows_ok,
												area => $area]);
	}

	$self->content($content);
}

sub shutdown_action {
	my $self = shift;
	my $cal = $self->{'shutdown_action'};
	if($cal) { &$cal(); }
}

1
