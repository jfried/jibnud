package Jibnu::Protocol::Server::HTTP::Request::Bad;

use strict;
use warnings;

use HTTP::Response;

use base qw(HTTP::Response);

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = $proto->SUPER::new(400, "Bad Request", @_);

	bless($self, $proto);
	
	return $self;
}

1;
