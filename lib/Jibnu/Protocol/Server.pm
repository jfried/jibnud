package Jibnu::Protocol::Server;

use strict;
use warnings;

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = { @_ };

	bless($self, $proto);
	return $self;
}

sub request {
	my $self = shift;
	return $self->{'request'};
}

sub set_status {
	my $self = shift;
	$main::threads_status{'thread-' . $self->{'counter'}} = shift;
}
1;
