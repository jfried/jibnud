package Jibnu::Plugin;

use strict;
use warnings;

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = { @_ };

	bless($self, $proto);

	return $self;
}

sub daemon {
	my $self = shift;
	return $self->{'daemon'};
}

sub escape_html {
	my $self = shift;
	my $s = shift;
	my $simple = shift;

	$s =~ s/&/&amp;/gms;
	$s =~ s/</&lt;/gms;
	$s =~ s/>/&gt;/gms;

	if(!$simple) {
		$s =~ s/\n/<br \/>/gms;
		$s =~ s/\t/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/gms;
	}

	return $s;
}



1;
