package Jibnu::FileTransfer::SFTP;

use strict;
use warnings;

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = { @_ };

	bless($self, $proto);

	return $self;
}

sub user {
	my $self = shift;
	return $self->{'user'};
}

sub server {
	my $self = shift;
	return $self->{'server'};
}

sub put {
	my $self = shift;
	my $local = shift;
	my $remote = shift;

	if($main::opts{'debug'}) {
		print("scp $local " . $self->user . '@' . $self->server . ":/" . $remote . ' >/dev/null 2>&1' . "\n");
	}
	system("scp $local " . $self->user . '@' . $self->server . ":/" . $remote . ' >/dev/null 2>&1');
}

1;
