package Jibnu::RemoteCommand::SSH;

use strict;
use warnings;

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = { @_ };

	bless($self, $proto);

	return $self;
}

sub user {
	my $self = shift;
	return $self->{'user'};
}

sub server {
	my $self = shift;
	return $self->{'server'};
}

sub execute {
	my $self = shift;
	my $cmd = shift;

	if($main::opts{"debug"}) {
		print("ssh " . $self->user . '@' . $self->server . " '" . $cmd . "' >/dev/null 2>&1\n");
	}
	system("ssh " . $self->user . '@' . $self->server . " '" . $cmd . "' >/dev/null 2>&1");
}

1;
