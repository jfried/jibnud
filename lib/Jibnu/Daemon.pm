package Jibnu::Daemon;

use strict;
use warnings;

use DBI;
use Cache::FileCache;

sub new {
	my $that = shift;
	my $proto = ref($that) || $that;
	my $self = { @_ };
	
	bless($self, $proto);
	
	return $self;
}

sub worker {
	my $self = shift;
	my $server = shift;
	my $counter = shift;
	
	# db verbindung aufbauen
	my $dsn = "DBI:mysql:database=" . $main::db_name . ";host=" . $main::db_host . ";port=" . $main::db_port;
	$self->{"dbh"} = DBI->connect($dsn, $main::db_user, $main::db_password, {
		AutoCommit => 1
	});
	$self->{"dbh"}->{"mysql_auto_reconnect"} = 1;

	# cache erstellen
	$self->{'cache'} = Cache::FileCache->new();

	# plugins einlesen
	my $plugin_dir = "$FindBin::Bin/../lib/Jibnu/Plugin";
	my @plugins = ();
	local *DH;
	opendir(DH, $plugin_dir);
	while(my $file = readdir(DH)) {
		next if($file =~ m/^\./);
		$file =~ m/^(.*?)\.pm$/;
		my $mod_name = "Jibnu::Plugin::$1";
		my $mod_file = "Jibnu/Plugin/$1.pm";
		eval {
			require $mod_file;
		};

		# todo: error handling
		if($@) { print $@; next; }

		push(@plugins, $mod_name);
	}
	closedir(DH);

	my $client;
	while(accept($client, $server)) {
		$client->autoflush(1);

		$self->{'request'} = Jibnu::Protocol::Server::HTTP->new(request => $client, counter => $counter, daemon => $self);

		# plugins laden
		# todo: nicht bei jedem request laden...
		foreach my $plug (@plugins) {
			$plug->new(daemon => $self)->init();
		}

		$self->{'response'} = $self->{'request'}->process();

		# todo: das muss die protocol klasse richtig zurueckgeben.
		print $client "HTTP/1.0 " . $self->{'response'}->as_string();
		$self->{'request'}->set_status('idle');
		
		print $client "\015\012";
		$client->flush();
		close($client);

		# dinge starten die laenger dauern
		eval { $self->{'response'}->shutdown_action(); };
	}
}

sub request {
	my $self = shift;
	return $self->{'request'};
}

sub response {
	my $self = shift;
	return $self->{'response'};
}

sub db {
	my $self = shift;
	return $self->{'dbh'};
}

sub cache {
	my $self = shift;
	return $self->{'cache'};
}

1;
