jQuery.noConflict()(function() {
	
	// external-content laden
	jQuery(document).ready(function() {
		window.setTimeout(function() {
			jQuery('.external-content').each(function(idx, obj) {
				load_external_content(obj, obj.getAttribute("src"));
			});
		}, 200);
	});

});


function load_external_content(obj, src) {
	jQuery.ajax({
		async: true,
		cache: true,
		type: 'GET',
		url: src,
		success: 
			function(data, status, xhr) {
				var keys = this.getAttribute("row").split(/\./);
				var _tmp = {};
				for (var i = 0; i < keys.length; i++) {
					_tmp = data[keys[i]];
					data = _tmp;
				};

				this.innerHTML = data;
			},
		error: 
			function(xhr, textStatus, errorThrown) {
				if(xhr.status == 502) { // bei einem 502 einfach nochmal probieren, und wieder, und wieder, ...
					load_external_content(this, this.getAttribute("src"));
				}
			},
		beforeSend: 
			function(xhr) {
				xhr.setRequestHeader('Accept', 'application/json');
			},
		context: obj
	});
}

