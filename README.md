# Jibnu Build Server

## REQUIREMENTS

* Text::Template::Simple
* Syntax::Highlight::Engine::Kate
* JSON
* JSON::XS
* forks
* Exception::Class (>=1.32)
* DBD::mysql
* IO::Socket::SSL
* Cache::Cache
* XML::Simple


## INSTALLATION

Open the file 'bin/jibnud.pl' and set the configuration parameters.

Create a (mysql) database and import the database schema from the file jibnu.sql.

### RUNNING

To start the server type:

```
bin/jibnud.pl --unix-socket /tmp/jibnud.sock --workers 10 --build-file http://svn.domain.tld/svn/build.xml
```

#### Options:

```
   --unix-socket  Path and Socketfile
   --workers      Workercount
   --build-file   XML Build Configuration
   --without-scm-update    Don't update scm tree at startup
   --debug        Start in debug mode
``` 

