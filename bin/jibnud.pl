#!/usr/bin/env perl

package main;

use forks;
use forks::shared;

use strict;
use warnings;

use Data::Dumper;

use Exception::Class;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;

use Jibnu::Daemon::UNIX;
use Jibnu::Protocol::Server::HTTP;
use Jibnu::Build::Configuration::XML;

use Jibnu::Exception::FileNotFound;

$| = 1;

# --- config vars
# !!! never ever forget the trailing slashes
our $template_dir = "$FindBin::Bin/../templates/";
our $build_dir = "/tmp/build/";
our $scm_checkout_dir = "/home/jibnu/scm/co/";

# database
our $db_host = "localhost";
our $db_port = 3306;
our $db_name = "jibnu";
our $db_user = "jibnu";
our $db_password = "kf8Cbaha";

# --- 


# --- check if all vars have the trailing slash
if($build_dir !~ m/\/$/) {
	$build_dir .= '/';
}

if($template_dir !~ m/\/$/) {
	$template_dir .= '/';
}

if($scm_checkout_dir !~ m/\/$/) {
	$scm_checkout_dir .= '/';
}
# ---

# --- shared vars
our %threads_status :shared;
# --- 

our %opts;
GetOptions(\%opts, "unix-socket=s", "workers=i", "debug", "build-file=s", "without-scm-update");

if( $opts{'debug'} ) {
	threads->debug( 1 );
}

# --- build konfiguration
print "[*] Loading configuration\n";
our $build_conf;
eval {
	$build_conf = Jibnu::Build::Configuration::XML->new(file => $opts{'build-file'});
};


my $e;
if($e = Exception::Class->caught('Jibnu::Exception::FileNotFound')) {
	print "[!] Could not retrieve configuration file. Check permissions.\n";
	exit 1;
}
# ---

# --- rechte im scm dir
chdir($scm_checkout_dir);
mkdir("t", 0777) or die($!);
rmdir("t");
chdir("$FindBin::Bin/../");
# ---

if(! -d $build_dir) { mkdir($build_dir, 0777); }

# build dir cleanen
print "[*] Cleaning $build_dir\n";
system("/bin/rm -rf " . $build_dir . "*");

# scms updaten
if(!$opts{'without-scm-update'}) {
	print "[*] Updating / Checking out all SCMs\n";
	foreach my $env ($build_conf->get_environments()) {
		print "   > $env\n";
		if( ! -d "$scm_checkout_dir$env") {
			# kompletter checkout
			mkdir("$scm_checkout_dir$env", 0777);
			chdir("$scm_checkout_dir$env");
			system("svn co " . $build_conf->get_env_scm($env) . " . >/dev/null 2>&1");
			if($? != 0) {
				print "[!!] WARN: Error checking out $env\n";
			}
		} else {
			# nur updaten
			chdir("$scm_checkout_dir$env");
			system("svn up >/dev/null 2>&1");
			if($? != 0) {
				print "[!!] WARN: Error on SCM update ($env)\n";
			}
		}
	}
}


print "[*] Starting service\n";
my $server = Jibnu::Daemon::UNIX->new(%opts);
$server->start();


